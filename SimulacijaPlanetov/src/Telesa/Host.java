package Telesa;

public interface Host {

    public double getAbsPosition(byte axis);

    public double getMass();
}
