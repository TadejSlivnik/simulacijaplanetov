package Telesa;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import Drugo.Koordinate;

public class Sonce extends Satelit {
    public Sonce(double diam, double masa, double posX, double posY) {
    	super(1, diam, 0, Color.white, masa, (Host)null);
        xPos = posX;
        yPos = posY;
    }

    public double getAbsPosition(byte axis) {
        if (axis == Koordinate.X)
            return xPos;
        else
            return yPos;
    }

    public void setMass(double newMass) {
        diameter*=newMass/mass;
        mass = newMass;
    }

    public void draw(Graphics2D comp2D) {
        super.draw(comp2D);
        BasicStroke pen = new BasicStroke(2F);
        comp2D.setStroke(pen);
    }
    public void Newton() {} // Sonce se ne premika, ne rab newtona
}
