package Telesa;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import main.Simulacija;
import Drugo.Koordinate;

abstract class Satelit implements Host{

    protected double xPos, yPos;

    protected double xVel = 0, yVel = 0;
    
    protected double xAcc = 0, yAcc = 0; 
    
    protected double dt;

    protected double diameter;

    protected double mass;

    protected final Color color;

    protected Host host;

    public Satelit(double dT, double diam, double radij, Color barva, double masa, Host sHost){
        dt = dT;
        diameter = diam;
        mass = masa;
        color = barva;
        host = sHost;
        
        xPos = Math.random()*radij;
        yPos = Math.sqrt(radij*radij - xPos*xPos);
    }

    public double getAbsPosition(byte axis) {
        if (axis == Koordinate.X)
            return xPos/Simulacija.getScale() + host.getAbsPosition(axis);
        else
            return yPos/Simulacija.getScale() + host.getAbsPosition(axis);
    }

    protected double getPosition(byte axis) {
    	if (axis == Koordinate.X)
            return xPos;
        else
            return yPos;
    }

    public double getTheta() {
        double theta = Math.atan(yPos/xPos);
        if (xPos < 0)
            theta = Math.PI + theta;
        else if (xPos > 0 && yPos < 0)
            theta = 2*Math.PI + theta;
        return theta;
    }

    public double getVel(byte axis) {
        if (axis == Koordinate.X)
            return xVel;
        else
            return yVel;
    }

    public double getAcc(byte axis) {
        if (axis == Koordinate.X)
            return xAcc;
        else
            return yAcc;
    }

    public abstract void Newton();

    
    public double getDiameter() {
        return diameter;
    }
    
    public double getMass() {
    	return mass;
    }

    public void draw(Graphics2D comp2D) {
        Ellipse2D.Double form =
        new Ellipse2D.Double(
        		(int)(getAbsPosition(Koordinate.X) - diameter/2/Simulacija.getScale()), 
        		(int)(getAbsPosition(Koordinate.Y) - diameter/2/Simulacija.getScale()), 
        		(int)diameter/Simulacija.getScale(), 
        		(int)diameter/Simulacija.getScale());
        comp2D.setColor(color);
        comp2D.fill(form);
    }
}
