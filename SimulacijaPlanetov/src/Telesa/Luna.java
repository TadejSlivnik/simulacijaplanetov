package Telesa;

import java.awt.Color;

import Drugo.Koordinate;

public class Luna extends Satelit {

    public Luna(double dT, double diam, double radij, Host mHost) {
    	super(dT, diam, radij, Color.lightGray, (double)1, mHost);
    }

    public void Newton() {
        double r = Math.sqrt(getPosition(Koordinate.X)*getPosition(Koordinate.X) + getPosition(Koordinate.Y)*getPosition(Koordinate.Y));
        double theta = getTheta() + Math.sqrt(host.getMass()/(r*r*r))*dt * 2; // hitrost lun
        xPos = r*Math.cos(theta);
        yPos = r*Math.sin(theta);
    }
}
