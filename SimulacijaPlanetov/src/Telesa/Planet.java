package Telesa;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;

import Drugo.Koordinate;
import main.Platno;
import main.Simulacija;


public class Planet extends Satelit {
	/**
	 * stevilo lun okrog planeta (od 0 do maxLun)
	 */
    private final byte numLun;

    private final byte maxLun = 3;
	
	private Luna[] lune;
	
	/**
	 * prejsne pozicije planeta
	 */
	private int[] prevXPos, prevYPos;
	
    private int trenutnaPikca = 0;

    private int count = 0;
    /**
     * stevilo pikc predn se zacnejo brisat
     */
    private final int steviloPikc = 100;
    
    public Planet(double dT, double diam, double radij, Color barva, double masa, Platno platno, Host mHost ){
        super(dT, diam, radij, Color.red , masa, mHost);
        
        prevXPos = new int[steviloPikc];
        prevYPos = new int[steviloPikc]; 
        for (int i = 0; i < steviloPikc; i+=1)
            prevXPos[i] = prevYPos[i] = 0;
        
        numLun = (byte) (Math.random()*maxLun);

        if (numLun > 0) {
            lune = new Luna[numLun];
            for (int i = 0; i < numLun; i+=1)
                lune[i] = new Luna(dt, Math.random()*6 + 3, diameter*(1 + 0.2*Math.random()), this);
        }

        double razdaljadoHOST = Math.sqrt(getPosition(Koordinate.X)*getPosition(Koordinate.X) + getPosition(Koordinate.Y)*getPosition(Koordinate.Y));
        /**
         * Gravitacija je 1
         */
        double force = host.getMass()*mass/(razdaljadoHOST*razdaljadoHOST);
        double theta = getTheta();
        /**
         * Cirkulacija
         */
        yVel = Math.sqrt(host.getMass()/razdaljadoHOST)*
               Math.sin(theta + Math.PI/2);
        xVel = Math.sqrt(host.getMass()/razdaljadoHOST)*
               Math.cos(theta + Math.PI/2);

        xAcc = force*Math.cos(theta + Math.PI)/mass;
        yAcc = force*Math.sin(theta + Math.PI)/mass;
    }
    
    public void Newton() {
    	
    	double force, razdaljadoHOST, theta;
    	
        for (int i = 0; i < 1/dt; ++i) {
            razdaljadoHOST = Math.sqrt(getPosition(Koordinate.X) * getPosition(Koordinate.X) + getPosition(Koordinate.Y) * getPosition(Koordinate.Y));

            force = mass*host.getMass()/(razdaljadoHOST*razdaljadoHOST);
            theta = getTheta();

            xAcc = force*Math.cos(theta + Math.PI)/mass;
            yAcc = force*Math.sin(theta + Math.PI)/mass;

            xPos+= xVel*dt + dt*dt*xAcc/2;
            yPos+= yVel*dt + dt*dt*yAcc/2;

            xVel+= xAcc*dt;
            yVel+= yAcc*dt;
            
            pikcastaOrbita2();
            
            for (int j = 0; j < numLun; ++j)
                lune[j].Newton();
        }
    }
    
    public void draw(Graphics2D comp2D) {
    	comp2D.setColor(color);
    	comp2D.fill(circle(
    			getAbsPosition(Koordinate.X),
    			getAbsPosition(Koordinate.Y),
    			diameter/Simulacija.getScale(),
    			getTheta() + Math.PI/2 ));
    	
    	pikcastaOrbita(comp2D);
    	
        for (int i = 0; i < numLun; i+=1)
            lune[i].draw(comp2D);
    }

    private void pikcastaOrbita(Graphics2D comp2D) {
        int x, y;

        for (int i = 0; i < steviloPikc; i+=1) {
            x = (prevXPos[i]/Simulacija.getScale() + (int)host.getAbsPosition(Koordinate.X));
            y = (prevYPos[i]/Simulacija.getScale() + (int)host.getAbsPosition(Koordinate.Y));
            comp2D.fill(new Ellipse2D.Float(x, y, 1, 1));
        }
    }
    
    private void pikcastaOrbita2() {
        if (++count > (int)(5/dt)) { // razdalja med pikcam
            count = 0;
        if (trenutnaPikca == steviloPikc) {  
            trenutnaPikca = 0;
        }

        prevXPos[trenutnaPikca] = (int)getPosition(Koordinate.X);
        prevYPos[trenutnaPikca++] = (int)getPosition(Koordinate.Y);
        }
    }
    
    private Area circle(double x, double y, double dia, double theta) {
        double xp = x - dia/2;
        double yp = y - dia/2;
        Ellipse2D.Double circle = new Ellipse2D.Double(xp, yp, dia, dia);
        Area CircleArea = new Area(circle);
        CircleArea.transform(AffineTransform.getRotateInstance(theta, x, y));
        return CircleArea;
    }
}
