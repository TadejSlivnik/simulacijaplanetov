package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Telesa.Sonce;

/**
 * Frame
 */
@SuppressWarnings("serial")
public class Simulacija extends JFrame implements ChangeListener, ActionListener{

	private JButton zoomOutGumb, zoomInGumb;
	
	private String zoomOutString = "Zoom Out", zoomInString = "Zoom In";
	
	private int zoomMAX = 7, zoomMIN = 0; // ne jt pod 0 pr minimumu
	
	private JPanel zoomPanel;
	
    private Platno platno;

    private static int xResolution = 1120, yResolution = 800;

    private Sonce sun;
    
    private JSlider slider;
    /**
     * kok bo ZOOM-a
     */
    private static int Scale = 2;
    /**
     * Masa sonca - maximum/minimum (za slider)
     */
    private static double masaMAX = 1.5, masaMIN = 0.5;

    @SuppressWarnings("static-access")
	public Simulacija(int num, double dt) {
        super("Simulacije planetov");

        sun = new Sonce(75, 1000, xResolution/2, yResolution/2);

        setSize(xResolution, yResolution);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        platno = new Platno(dt, num, sun);
        platno.setBackground(Color.black);

        BorderLayout MasterPanelLayout = new BorderLayout();
        Container pane = getContentPane();
        pane.setLayout(MasterPanelLayout);
        pane.add(platno, BorderLayout.CENTER);

        slider = new JSlider(JSlider.VERTICAL, (int)(sun.getMass()*masaMIN), (int)(sun.getMass()*masaMAX), (int)sun.getMass());
        slider.setMajorTickSpacing((int)(sun.getMass()*(masaMAX - masaMIN)/20));
        slider.setMinorTickSpacing((int)(sun.getMass()*(masaMAX - masaMIN)/40));
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.addChangeListener(this);
        pane.add(slider, MasterPanelLayout.EAST);
        
        zoomPanel = new JPanel();
        
        zoomOutGumb = new JButton(zoomInString);
        zoomOutGumb.addActionListener(this);
        zoomPanel.add(zoomOutGumb);
        
        zoomInGumb = new JButton(zoomOutString);
        zoomInGumb.addActionListener(this);
        zoomPanel.add(zoomInGumb);
        
        pane.add(zoomPanel, MasterPanelLayout.NORTH);
        
        setContentPane(pane);
        setVisible(true);
    }

    public Platno getPanel() {
        return platno;
    }
    
    public static int getScale() {
        return Scale;
    }
    
    public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider)e.getSource();
        if (source.getValueIsAdjusting() != true) // ne spreminja mase sonca dokler ne izpustimo slider
        	sun.setMass((double)slider.getValue());
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
        if (command.equals(zoomInString)) {
        	if (Scale - 1 > zoomMIN){Scale -= 1;}
        }
        else if (command.equals(zoomOutString)) {
        	if (Scale + 1 <= zoomMAX){Scale += 1;}
        }
		
	}
}
