package main;

public class Main {
	/**
	 * cas / hitrost
	 */
	private static double dt = 0.001;
	
    public Main() { }
    
    public static void main(String[] args) {
        Simulacija Frame;
        /**
         * Stevilo planetov, okol 10 zacne pocasnej delat.
         * Priporocena max meja je 30
         */
        int numPlanetov = 7;
        Frame = new Simulacija(numPlanetov, dt);
        
        while (true) {
        	Frame.getPanel().repaint();
        }
    }
}