package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import Telesa.Planet;
import Telesa.Sonce;

/**
 * Sonce, Planeti
 */
@SuppressWarnings("serial")
public class Platno extends JPanel {

    private Planet[] planet;
    
    private double minDiameter = 15, maxDiameter = 45;

    private final int numberOfPlanets;

    private boolean[] orbitaVzeta;
    
    private Sonce sonce;

    public Platno(double dt, int numPlanetov, Sonce sun){
        sonce = sun;
        numberOfPlanets = numPlanetov;
        
        orbitaVzeta = new boolean[numberOfPlanets];
        for (int i = 0; i < numberOfPlanets; i+=1)
            orbitaVzeta[i] = false;
        
        planet = new Planet[numberOfPlanets];
        
        for (int i = 0; i < numberOfPlanets; i+=1) {
            double diameter = minDiameter + Math.random()* (maxDiameter - minDiameter);
            planet[i] = new Planet(dt, diameter, orbita(), Color.RED, diameter, this, sun);
        }
    }
    
   /**
    * Izbira orbite dokler ne najde ene nezasedene
    */
    private double orbita() {
        int poizkus = (int)(Math.random()*numberOfPlanets);

        while (true) {
            if (!orbitaVzeta[poizkus]) {
                orbitaVzeta[poizkus] = true;
                break;
            }
            else
                poizkus = (int)(Math.random()*numberOfPlanets);
        }

        return 1.5*sonce.getDiameter() + poizkus*maxDiameter + maxDiameter*1.5;
    }

    public void paintComponent(Graphics comp) {
        super.paintComponent(comp);
        Graphics2D comp2D = (Graphics2D) comp;

        sonce.draw(comp2D);
        
        for (int i = 0; i < numberOfPlanets; i+=1) {
            planet[i].Newton();
            planet[i].draw(comp2D);
        }
    }
}
