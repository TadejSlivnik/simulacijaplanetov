##### Avtor: Tadej Slivnik #####

# README #

Repozitorij vsebuje projekt pri predmetu Programiranje 2. Napisan je v programskem jeziku Java.

### Simulacija Planetov ###

Program narejen v javi simulira gibanje naključno velikih planetov okoli sonca. Uporabnik lahko spreminja maso sonca ter velikost platna. Na začetku se planeti gibljejo po krožnicah. Planeti imajo lahko tudi več lun.

### Zahteve ###

* Java
* Program se požene z datoteko src/main/Main.java

### Kontakt ###

* tadej.slivnik@student.fmf.uni-lj.si
* slivnik92@outlook.com